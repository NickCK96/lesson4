<?php
require_once 'list.php';
require_once 'classes/Product.php';
require_once  'classes/Drink.php';
require_once  'classes/Meal.php';
require_once 'classes/HtmlWriter.php';



$mealObjs = [];
foreach ($meals as $mealArray){
    $mealObjs[] = new Meal($mealArray['title'],$mealArray['price'],
    $mealArray['ingredients'],$mealArray['vegetarian'],$mealArray['weight']);

}$drinkObjs = [];
foreach ($drinks as $drink){
    $drinkObjs[] = new Drink($drink['title'],$drink['price'],$drink['volume']);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>General menu</title>
    <style>
        li{
            list-style-type: none;

        }
        a{
            text-decoration: none;
            color: cornflowerblue;
        }
    </style>
</head>
<body>
<center>
    <h1>Welcome! </h1>
    <h3>General menu</h3>
    <div>
        <ul>
            <center>

                <li>
                    <a href="drinks.php">Drinks</a>
                </li>
                <li>
                    <a href="vegetarian.php">Vegetarian dish</a>

                </li>
            </center>
        </ul>
    </div>
    <div>

        <?php foreach ($mealObjs as $mealObj):?>
        <?= HtmlWriter::writeDish($mealObj)?>
        <?php endforeach;?>
    </div>
    <hr>
    <div>
        <?php foreach ($drinkObjs as $drinkObj):?>
        <?= HtmlWriter::writeDrink($drinkObj)?>
        <?php endforeach;?>
    </div>
</center>
</body>
</html>
