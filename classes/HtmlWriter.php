<?php


class HtmlWriter
{
    static public function writeTitlePrice(Product $product){
        $str = '<div>';
        $str .= '<h2>'. $product->getTitle() . '</h2>';
        $str .= '<h3> Price:'. $product->getPrice() . '</h3>';
        return $str;
    }
    public function writeDish(Meal $meal){
        $str = '<div>';
        $str .= self::writeTitlePrice($meal);
        $str .= 'Ingredients ' . $meal->getIngredients();
        $str .= 'Weight ' . $meal->getWeight();
        $vegetarian = 'No';
        if($meal ->isVegetarian()){
            $vegetarian = 'Yes';
        }
        $str .= 'Vegetarian' . $vegetarian;
        $str .= '</div>';
        return $str;

    }public function writeDrink(Drink $drink){
        $str = '<div>';
        $str .= self::writeTitlePrice($drink);
        $str .= 'Volume ' . $drink->getVolume();
        $str .= '</div>';
        return $str;

    }
}