<?php


class Product
{
    protected $title = '';
    protected $price = 0;


    public function __construct($title, $price)
    {
        $this->title = $title;
        $this->price = $price;
    }


    public function getTitle()
    {
        return $this->title;
    }


    public function getPrice()
    {
        return $this->price;
    }
}