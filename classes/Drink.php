<?php


class Drink extends Product
{
    protected $volume = 0;


    public function __construct($title,$price, $volume)
    {
        parent::__construct($title, $price);
        $this->volume = $volume;
    }


    public function getVolume()
    {
        return $this->volume;
    }

}