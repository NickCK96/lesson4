<?php


class Meal extends Product
{
    protected $ingredients = '';
    protected $vegetarian = false;
    protected $weight = 0;


    public function __construct($title, $price, $ingredients, $vegetarian, $weight)
    {
        parent::__construct($title,$price);
        $this->ingredients = $ingredients;
        $this->vegetarian = $vegetarian;
        $this->weight = $weight;
    }


    public function getIngredients()
    {
        return $this->ingredients;
    }


    public function isVegetarian()
    {
        return $this->vegetarian;
    }


    public function getWeight()
    {
        return $this->weight;
    }

}