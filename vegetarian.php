<?php
require_once 'list.php';
require_once 'classes/Product.php';

require_once  'classes/Meal.php';
require_once 'classes/HtmlWriter.php';



$mealObjs = [];
foreach ($meals as $mealArray) {
    $mealObjs[] = new Meal($mealArray['title'], $mealArray['price'],
        $mealArray['ingredients'], $mealArray['vegetarian'], $mealArray['weight']);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vegetarian menu</title>
    <style>
        li{
            list-style-type: none;

        }
        a{
            text-decoration: none;
            color: cornflowerblue;
        }
    </style>
</head>
<body>
<center>
    <h1>Welcome! </h1>
    <h3>General menu</h3>
    <div>
        <ul>
            <center>

                <li>
                    <a href="drinks.php">Drinks</a>
                </li>
                <li>
                    <a href="index.php">General menu</a>

                </li>
            </center>
        </ul>
    </div>
    <div>

        <?php foreach ($mealObjs as $mealObj):?>
        <?php if($mealObj->isVegetarian()) :?>
        <?= HtmlWriter::writeDish($mealObj)?>
        <?endif;?>
        <?php endforeach;?>
    </div>
    <hr>

</center>
</body>
</html>
